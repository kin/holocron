# KIN SGI 
 
KIN Standard Gateway Interface :


"Ruby application Call Stack" style of interface

It returns a function that accept one uniq parameter "env"
and return an array of 3 elements [status,header,body] of type :int,:hash,:enumerable
makes it chainable

requirement
a call method taking env
a run method to run the lambda function for each request
see also [*](https://www.slideshare.net/DonSchado/froscon-rack)



 
```perl
#!/usr/bin/perl
use strict;
use warnings;
 
use Data::Dumper qw(Dumper);
$Data::Dumper::Sortkeys = 1;
 
my $app = sub { # call method
  my $env = shift;
  return [
    '200',
    [ 'Content-Type' => 'text/plain' ],
    [ Dumper $env ],
  ];
};
```
