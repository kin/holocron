#!/usr/bin/env perl

my $CORE = 'HLR';
my $core = lc $CORE;
my $pp = substr($core,0,2);

my $HLR_PATH = (defined $ENV{"${CORE}_PATH"}) ? $ENV{"${CORE}_PATH"} : $ENV{HOME}."/.${pp}rings";

my $map = {
'echohip6' => 'QmeB4r7gjrd25H1Q9QRpXTXuLSUGSPtxWLq4i29EBA9Ug9',
'hrl_install.sh' => 'QmZT5myTDrv96aDDakDVoZ2rp9zzdV7Pfj1JbAknJsdQfQ',
'nop' => 'QmVGTcK9qovFM8UKfwE8DhMdPDE3H57jwSMwZtkAYK2qWT'
}
my $app = shift;
if (! defined $map->{$app}) {
      printf "app: %s is not registered\n";
      exit -$$;
}


&init($HLR_PATH) if (! -d $HLR_PATH);

my $appdir = &get_app_path($app);
if (! -d $appdir) { # app is not local !
  my  $url = sprintf'http://127.0.0.1:8080/ipfs/%s',$map->{$app};
  &get_app($appdir,$url);
}
# -----------------------------------------------------
if (-e "$appdir/start.pl") {
   do "$appdir/start.pl"; # execute smart contact ...
} else (-e "$appdir/runme.sh") {
 
} elsif (-e "$appdir/README.md") {
  system sprintf 'xdg-open %s/README.md',$appdir;
} else {
  printf "app: %s installed in %s\n",$app,$appdir;
}
# -----------------------------------------------------
exit $?;

sub get_app {
 my ($dir,$url) = @_;
 if (! -e $dir) {
   &mkdir_p($dir);
 }
 chdir $dir;
 system $curlit
#curl -X POST "http://127.0.0.1:5001/api/v0/get?arg=QmVGTcK9qovFM8UKfwE8DhMdPDE3H57jwSMwZtkAYK2qWT&output=foo&compress=true"
 
}
sub get_app_path {
 # TBD check if up-to-date ..
 my $file = sprintf'%s/app/%s',$HLR_PATH,$_[0];
 return $file;
}

sub init {
 my $intalldir = shift;
 printf "info: %s: %s\n",caller,$installdir;
 my $appdir = &get_app_path('hlr_install.sh',$url);
 &get_app($appdir,$url);
 chdir $appdir;
 my $status;
 if (-e 'hlr_install.sh') {
   $status = system "sh -xe hlr_install.sh";
 }
 return $status;
}

sub mkdir_p {
   my $dirname = shift;
   my @dir = split /\//,$dirname;
   my $path = '';
   for (0 .. $#dir) {
      $path .= $dir[$_];
      mkdir $path or last;
      $path .= '/';
   }
   return $path;
}
1;
